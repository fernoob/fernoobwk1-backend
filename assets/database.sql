CREATE TABLE IF NOT EXISTS `fernoob_wk1`.`customer` (
  `id_customer` INT NOT NULL AUTO_INCREMENT,
  `customer` VARCHAR(200) NULL,
  `cpf` VARCHAR(50) NULL,
  `birthday` VARCHAR(8) NULL,
  `email` VARCHAR(100) NULL,
  `address` VARCHAR(200) NULL,
  `address2` VARCHAR(200) NULL,
  `cep` VARCHAR(10) NULL,
  `number` VARCHAR(45) NULL,
  `city` VARCHAR(100) NULL,
  `cid` VARCHAR(20) NULL,
  PRIMARY KEY (`id_customer`))
ENGINE = InnoDB

CREATE TABLE IF NOT EXISTS `fernoob_wk1`.`product` (
  `id_product` INT NOT NULL AUTO_INCREMENT,
  `product` VARCHAR(100) NULL,
  `price` DOUBLE(10,2) NULL,
  `pid` VARCHAR(20) NULL,
  PRIMARY KEY (`id_product`))
ENGINE = InnoDB

CREATE TABLE IF NOT EXISTS `fernoob_wk1`.`selling` (
  `id_selling` INT NOT NULL AUTO_INCREMENT,
  `id_customer` INT NOT NULL,
  `date` VARCHAR(12) NULL,
  PRIMARY KEY (`id_selling`),
  INDEX `fk_selling_customer_idx` (`id_customer` ASC),
  CONSTRAINT `fk_selling_customer`
    FOREIGN KEY (`id_customer`)
    REFERENCES `id19983779_fernoobwk`.`customer` (`id_customer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB

CREATE TABLE IF NOT EXISTS `fernoob_wk1`.`selling_has_product` (
  `id_selling_has_product` INT NOT NULL AUTO_INCREMENT,
  `id_selling` INT NOT NULL,
  `id_product` INT NOT NULL,
  `qty` DOUBLE(10,2) NULL,
  PRIMARY KEY (`id_selling_has_product`),
  INDEX `fk_selling_has_product_product1_idx` (`id_product` ASC),
  INDEX `fk_selling_has_product_selling1_idx` (`id_selling` ASC),
  CONSTRAINT `fk_selling_has_product_selling1`
    FOREIGN KEY (`id_selling`)
    REFERENCES `id19983779_fernoobwk`.`selling` (`id_selling`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_selling_has_product_product1`
    FOREIGN KEY (`id_product`)
    REFERENCES `id19983779_fernoobwk`.`product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
