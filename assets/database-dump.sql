INSERT INTO `customer` (`id_customer`, `cid`, `customer`, `cpf`, `birthday`, `email`, `address`, `address2`, `cep`, `number`, `city`)
VALUES
	(1, 'USER', 'Fernando Novo', '12345678910', '19970901', 'teste@teste.com', 'Cidade Das Laranjeiras', 'Rua dos pêssegos', '12345-100', '1', 'Citry'),
	(3, '123', 'Teste', '026.653.672-70', '01091997', 'rfbcj@hotmail.com', 'rua poeta manel', 'Não tem', '13310-140', '38', 'Lisboa');

INSERT INTO `product` (`id_product`, `product`, `price`, `pid`)
VALUES
	(2, 'Mouse Gamer', 2000.00, 'mg200'),
	(3, 'Monitor portátil1', 2001.00, 'mp1'),
	(5, 'Novo produto', 200.00, NULL),
	(6, 'Monitor portátil', 200.00, NULL),
	(7, 'Bag', 20.00, NULL);

INSERT INTO `selling` (`id_selling`, `id_customer`, `date`)
VALUES
	(1, 1, '20220901');

INSERT INTO `selling_has_product` (`id_selling_has_product`, `id_selling`, `id_product`, `qty`)
VALUES
	(2, 1, 2, 2.00),
	(3, 1, 3, 1.00);
