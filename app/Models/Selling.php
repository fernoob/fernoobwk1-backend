<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Selling
 *
 * @property int $id_selling
 * @property int $id_customer
 * @property string|null $date
 *
 * @property Customer $customer
 * @property Collection|Product[] $products
 *
 * @package App\Models
 */
class Selling extends Model
{
	protected $table = 'selling';
	protected $primaryKey = 'id_selling';
	public $timestamps = false;

	protected $casts = [
		'id_customer' => 'int'
	];

	protected $fillable = [
		'id_customer',
		'date'
	];

	public function customer()
	{
		return $this->belongsTo(Customer::class, 'id_customer');
	}

	public function products()
	{
		return $this->belongsToMany(Product::class, 'selling_has_product', 'id_selling', 'id_product')
					->withPivot('qty');
	}
}
