<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 *
 * @property int $id_customer
 * @property string|null $customer
 * @property string|null $cid
 * @property string|null $cpf
 * @property string|null $birthday
 * @property string|null $email
 * @property string|null $address
 * @property string|null $address2
 * @property string|null $cep
 * @property string|null $number
 * @property string|null $city
 *
 * @property Collection|Selling[] $sellings
 *
 * @package App\Models
 */
class Customer extends Model
{
	protected $table = 'customer';
	protected $primaryKey = 'id_customer';
	public $timestamps = false;

	protected $fillable = [
		'customer',
		'cid',
		'cpf',
		'birthday',
		'email',
		'address',
		'address2',
		'cep',
		'number',
		'city'
	];

	public function sellings()
	{
		return $this->hasMany(Selling::class, 'id_customer');
	}
}
