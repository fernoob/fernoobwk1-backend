<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @property int $id_product
 * @property string|null $product
 * @property string|null $pid
 * @property float|null $price
 *
 * @property Collection|Selling[] $sellings
 *
 * @package App\Models
 */
class Product extends Model
{
	protected $table = 'product';
	protected $primaryKey = 'id_product';
	public $timestamps = false;

	protected $casts = [
		'price' => 'float'
	];

	protected $fillable = [
		'product',
		'price',
		'pid'
	];

	public function sellings()
	{
		return $this->belongsToMany(Selling::class, 'selling_has_product', 'id_product', 'id_selling')
					->withPivot('qty');
	}
}
