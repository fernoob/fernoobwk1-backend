<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SellingHasProduct
 *
 * @property int $id_selling_has_product
 * @property int $id_selling
 * @property int $id_product
 * @property float|null $qty
 *
 * @property Product $product
 * @property Selling $selling
 *
 * @package App\Models
 */
class SellingHasProduct extends Model
{
	protected $table = 'selling_has_product';
	protected $primaryKey = 'id_selling_has_product';
	public $timestamps = false;

	protected $casts = [
		'id_selling' => 'int',
		'id_product' => 'int',
		'qty' => 'float'
	];

	protected $fillable = [
		'qty'
	];

	public function product()
	{
		return $this->belongsTo(Product::class, 'id_product');
	}

	public function selling()
	{
		return $this->belongsTo(Selling::class, 'id_selling');
	}
}
