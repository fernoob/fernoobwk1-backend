<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SellingHasProduct;

class SellingHasProductController extends Controller
{
  public function store(Request $request){
    return SellingHasProduct::create($request->all());
  }

  public function update(Request $request, $id){
    $selling = SellingHasProduct::findOrFail($id);
    $selling->update($request->all());

    return $selling;
  }

  public function delete(Request $request, $id){
    $selling = SellingHasProduct::findOrFail($id);
    $selling->delete();

    return 204;
  }
}
