<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Selling;

class SellingController extends Controller {
  public function index(){
      return Selling::with('products')->with('customer')->get();
  }

  public function show($id){
    return Selling::with('products')->with('customer')->find($id);
  }

  public function store(Request $request){
    return Selling::create($request->all());
  }

  public function update(Request $request, $id){
    $selling = Selling::findOrFail($id);
    $selling->update($request->all());

    return $selling;
  }

  public function delete(Request $request, $id){
    $selling = Selling::findOrFail($id);
    $selling->delete();

    return 204;
  }
}
