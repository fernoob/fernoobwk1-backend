<?php

Route::middleware(['cors'])->group(function () {

  Route::get('customer', 'App\Http\Controllers\CustomerController@index');
  Route::get('customer/{customer}', 'App\Http\Controllers\CustomerController@show');
  Route::post('customer', 'App\Http\Controllers\CustomerController@store');
  Route::put('customer/{customer}', 'App\Http\Controllers\CustomerController@update');
  Route::delete('customer/{customer}', 'App\Http\Controllers\CustomerController@delete');

  Route::get('product', 'App\Http\Controllers\ProductController@index');
  Route::get('product/{product}', 'App\Http\Controllers\ProductController@show');
  Route::post('product', 'App\Http\Controllers\ProductController@store');
  Route::put('product/{product}', 'App\Http\Controllers\ProductController@update');
  Route::delete('product/{product}', 'App\Http\Controllers\ProductController@delete');

  Route::get('selling', 'App\Http\Controllers\SellingController@index');
  Route::get('selling/{selling}', 'App\Http\Controllers\SellingController@show');
  Route::post('selling', 'App\Http\Controllers\SellingController@store');
  Route::put('selling/{selling}', 'App\Http\Controllers\SellingController@update');
  Route::delete('selling/{selling}', 'App\Http\Controllers\SellingController@delete');

  Route::post('sellinghasproduct', 'App\Http\Controllers\SellingHasProductController@store');
  Route::put('sellinghasproduct/{sellinghasproduct}', 'App\Http\Controllers\SellingHasProductController@update');
  Route::delete('sellinghasproduct/{sellinghasproduct}', 'App\Http\Controllers\SellingHasProductController@delete');

});
